import json
import socket
import sys


class Piece(object):

    STRAIGHT, BEND = 's', 'b'

    def __init__(self, data):
        vars(self).update(data)
        if 'length' in data:
            self.kind = self.STRAIGHT
        else:
            self.kind = self.BEND

    @property
    def is_straight(self):
        return self.kind == self.STRAIGHT

    @property
    def is_bend(self):
        return self.kind == self.BEND

    def __repr__(self):
        if self.is_straight:
            return 'Piece(%s, %s)' % (self.kind, self.length)
        else:
            return 'Piece(%s, %s, %s)' % (self.kind, self.radius, self.angle)


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.car = None
        self.pieces = []
        self.lanes = []
        self.cars = []

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def _filter_my_car(self, data):
        my_car = self.car
        for d in data:
            car = d['id']
            if car['name'] == my_car['name']:
                return d
        return {}

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_your_car(self, data):
        self.car = data
        print("My car: ", data['name'], data['color'])
        self.ping()

    def on_game_init(self, data):
        self.pieces = [Piece(d) for d in data['race']['track']['pieces']]
        self.lanes = data['race']['track']['lanes']
        self.cars = data['race']['cars']
        self.race_session = data['race']['raceSession']
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1.0)

    def on_car_positions(self, data):
        pos = self._filter_my_car(data)
        piece_pos = pos['piecePosition']
        angle = pos['angle']
        i = piece_pos['pieceIndex']
        d = piece_pos['inPieceDistance']
        lap = piece_pos['lap']
        piece = self.pieces[i]
        print(i, piece, ', dist ', d, ', angle', angle)
        next_piece = self.pieces[(i + 1) % len(self.pieces)]
        if (piece.is_bend or
                abs(angle) > 5 or
                piece.is_straight and next_piece.is_bend and
                d > piece.length / 2.0):
            if lap > 0 and i == 3 or abs(angle) > 20:
                self.throttle(0.0)
            else:
                self.throttle(0.5)
        else:
            self.throttle(1.0)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_spawn(self, data):
        print("spawn!")
        self.throttle(1.0)

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
